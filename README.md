# #深入浅出学习eTs#

#### 介绍
带大家深入浅出学习eTs，从最简单的环境大家到大家能独立进行开发，本内容预计40+课程，希望大家能得到很多收获

#### 文章架构
[ #深入浅出学习eTs#（一）模拟器/真机环境搭建](Lesson1_搭建环境/README.md)

[#深入浅出学习eTs#（二）拖拽式UI](Lesson2_拖拽式UI/README.md)

[#深入浅出学习eTs#（三）UI布局](Lesson3_UI布局/README.md)

[#深入浅出学习eTs#（四）登陆界面UI](Lesson4_登陆界面UI/README.md)

[#深入浅出学习eTs#（五）eTs语言初识](Lesson5_eTs语言初识/README.md)

[#深入浅出学习eTs#（六）编写eTs第一个控件](Lesson6_编写eTs第一个控件/README.md)

[#深入浅出学习eTs#（七）判断密码是否正确](Lesson7_判断密码是否正确/README.md)

[#深入浅出学习eTs#（八）“赌博”小游戏](Lesson8_“赌博”小游戏/README.md)

[#深入浅出学习eTs#（九）变红码？专属二维码生成](Lesson9_专属二维码生成/README.md)

[#深入浅出学习eTs#（十）蓝药丸还是红药丸](Lesson10_蓝药丸还是红药丸/README.md)

[#深入浅出学习eTs#（十一）别忘了吃药喔](Lesson11_别忘了吃药喔/README.md)

[#深入浅出学习eTs#（十二）您的电量不足](Lesson12_您的电量不足/README.md)

[#深入浅出学习eTs#（十三）滑动块提示](Lesson13_滑动块提示/README.md)

[#深入浅出学习eTs#（十四）我的专属广告](Lesson14_我的专属广告/README.md)

[#深入浅出学习eTs#（十五）九宫格密码锁](Lesson15_九宫格密码锁/README.md)

[#深入浅出学习eTs#（十六）主人来电话了](Lesson16_主人来电话了/README.md)

[#深入浅出学习eTs#（十七）远端模拟器](Lesson17_远端模拟器/README.md)

[#深入浅出学习eTs#（十八）专属浏览器](Lesson18_专属浏览器/README.md)

[#深入浅出学习eTs#（十九）TCP聊天室](Lesson19_TCP聊天室/README.md)
