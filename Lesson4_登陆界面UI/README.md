#  #深入浅出学习eTs#（四）登陆界面UI

#### 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、明确目标

经过前面两章的学习，大家对Super Visual应该有了一个较为简单的认识，这一章就把前面的知识点串一下，使用Ark UI(Super Visual)赖模仿一个QQ的登陆界面，如下图

![1](Image/1.png)

针对这个界面，我们提取出来主要的内容

* QQ头像：可以使用Image控件
* 帐号：使用输入框
* 密码：使用INPUT
* 登录按钮：使用Button

## 二、分析目标

通过（一）的内容我们提取出主要需要的部分，现在来分析一下布局：

#### QQ头像：左右居中

#### 帐号输入框：左侧有一个小图标，右侧是输入，是左右布局

#### 密码输入框：同上

#### 登陆按钮：居中

### 三、Image的使用

此时我们需要使用Image控件，先看一下image控件的介绍

# Image

图片组件，支持本地图片和网络图片的渲染展示。

### ImageFit

| 名称      | 描述                                                         |
| :-------- | :----------------------------------------------------------- |
| Cover     | 保持宽高比进行缩小或者放大，使得图片两边都大于或等于显示边界。 |
| Contain   | 保持宽高比进行缩小或者放大，使得图片完全显示在显示边界内。   |
| Fill      | 不保持宽高比进行放大缩小，使得图片充满显示边界。             |
| None      | 保持原有尺寸显示。                                           |
| ScaleDown | 保持宽高比显示，图片缩小或者保持不变。                       |

### ImageInterpolation

| 名称   | 描述                                               |
| :----- | :------------------------------------------------- |
| None   | 不使用插值图片数据。                               |
| High   | 插值图片数据的使用率高，可能会影响图片渲染的速度。 |
| Medium | 插值图片数据的使用率中。                           |
| Low    | 插值图片数据的使用率低。                           |

### ImageRenderMode

| 名称     | 描述                                       |
| :------- | :----------------------------------------- |
| Original | 按照原图进行渲染，包括颜色。               |
| Template | 将图片渲染为模板图片，忽略图片的颜色信息。 |

我们先在软件中放置一个Image

![1](Image/2.png)

在此处选择已经保存到本地的头像图片

![1](Image/3.png)

该文件路径需要是工程内的路径（因为我这里是在lesson3的内容上改的，所以显示lesson3，这个不重要）

![1](Image/4.png)

这里一般放置在media目录下，此时再导入该路径

![1](Image/5.png)

此时实现了Image的显示部分

## 四、Image的布局

![1](Image/6.png)

如果是使用这个进行移动的话，不能完全靠中，且极易发生位移改变，这里使用之前学习到的flex控件

![1](Image/7.png)

这里选择在Flex中塞入一个Image，且把Flex设置为左右居中，上下居中，此时实现了图片的居中

## 五、QQ帐号输入框

选择使用Row控件实现，因为是水平对齐

# TextInput

可以输入单行文本并支持响应输入事件的组件。

参数

| 参数名       | 参数类型                                                     | 必填 | 默认值 | 参数描述               |
| :----------- | :----------------------------------------------------------- | :--- | :----- | :--------------------- |
| placeholder  | string \| [Resource](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/ui/ts-types.md/#resource类型) | 否   | -      | 无输入时的提示文本。   |
| text         | string \| [Resource](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/ui/ts-types.md/#resource类型) | 否   | -      | 设置提示文本的当前值。 |
| controller8+ | [TextInputController](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-textinput.md/#textinputcontroller8) | 否   | -      | 设置TextInput控制器。  |

- EnterKeyType枚举说明

  | 名称                | 描述               |
  | :------------------ | :----------------- |
  | EnterKeyType.Go     | 显示Go文本。       |
  | EnterKeyType.Search | 显示为搜索样式。   |
  | EnterKeyType.Send   | 显示为发送样式。   |
  | EnterKeyType.Next   | 显示为下一个样式。 |
  | EnterKeyType.Done   | 标准样式。         |

- InputType枚举说明

  | 名称               | 描述                 |
  | :----------------- | :------------------- |
  | InputType.Normal   | 基本输入模式。       |
  | InputType.Password | 密码输入模式。       |
  | InputType.Email    | e-mail地址输入模式。 |
  | InputType.Number   | 纯数字输入模式。     |

在placeholder参数进行设置后得到如下：

![1](Image/10.png)

此时得到帐号的输入

## 六、QQ密码输入框

密码输入框如法炮制，不过对应的InputType类型选择为InputType.Password，变为密码输入模式

![1](Image/12.png)

此时已经出现了QQ帐号的输入和QQ密码的输入（图片随便选的，这个不重要）

## 六、登陆按钮

![1](Image/13.png)

因为按钮默认是居中的，所以调整好大小和字体大小和颜色以及间距就可以了

## 七、转换为eTs文件/模拟器展示

![1](Image/14.png)

转换为模拟器后发现与设计草稿一致，此时可以直接点击模拟器中的输入框

![1](Image/15.png)

是可以点击和模拟的
