#  #深入浅出学习eTs#（十五）九宫格密码锁

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、需求分析

![1](Image/1.png)

相信没有人没有使用过九宫格解锁吧，从智能机开始迸发的时期到现在，我们本期就要做一个自己的密码锁

* 密码正确可进入
* 提示密码错误
* 可修改密码

## 二、控件介绍

# PatternLock[OpenAtom OpenHarmony](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-patternlock.md/)

图案密码锁组件，以九宫格图案的方式输入密码，用于密码验证场景。手指在PatternLock组件区域按下时开始进入输入状态，手指离开屏幕时结束输入状态完成密码输入。

> **说明：**
>
> 该组件从API Version 9开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

## 接口

PatternLock(controller?: PatternLockController)

**参数:**

| 参数名     | 参数类型                                                     | 必填 | 描述                                                |
| :--------- | :----------------------------------------------------------- | :--- | :-------------------------------------------------- |
| controller | [PatternLockController](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-patternlock.md/#patternlockcontroller) | 否   | 设置PatternLock组件控制器，可用于控制组件状态重置。 |

## 属性

不支持除backgroundColor以外的通用属性设置。

| 名称            | 参数类型                                                     | 描述                                                         |
| :-------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| sideLength      | [Length](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#length) | 设置组件的宽度和高度（宽高相同）。设置为0或负数等非法值时组件不显示。 默认值：300vp |
| circleRadius    | [Length](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#length) | 设置宫格中圆点的半径。 默认值：14vp                          |
| regularColor    | [ResourceColor](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#resourcecolor) | 设置宫格圆点在“未选中”状态的填充颜色。 默认值：Color.Black   |
| selectedColor   | [ResourceColor](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#resourcecolor) | 设置宫格圆点在“选中”状态的填充颜色。 默认值：Color.Black     |
| activeColor     | [ResourceColor](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#resourcecolor) | 设置宫格圆点在“激活”状态的填充颜色（“激活”状态为手指经过圆点但还未选中的状态）。 默认值：Color.Black |
| pathColor       | [ResourceColor](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#resourcecolor) | 设置连线的颜色。 默认值：Color.Blue                          |
| pathStrokeWidth | number \| string                                             | 设置连线的宽度。设置为0或负数等非法值时连线不显示。 默认值：34vp |
| autoReset       | boolean                                                      | 设置在完成密码输入后再次在组件区域按下时是否重置组件状态。设置为true，完成密码输入后再次在组件区域按下时会重置组件状态（即清除之前输入的密码）；反之若设置为false，则不会重置组件状态。 默认值：true |

## 事件

除支持[通用事件](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-universal-events-click.md/)外，还支持以下事件：

| 名称                                                        | 描述                                                         |
| :---------------------------------------------------------- | :----------------------------------------------------------- |
| onPatternComplete(callback: (input: Array<number>) => void) | 密码输入结束时触发该回调。 input: 与选中宫格圆点顺序一致的数字数组，数字为选中宫格圆点的索引值（第一行圆点从左往右依次为0,1,2，第二行圆点依次为3,4,5，第三行圆点依次为6,7,8）。 |

![1](Image/1.gif)

```ts
@Entry @Component struct PatternLockTest {

  build() {
    Column({space: 10}) {
      PatternLock(this.patternLockController)
    }
    .width('100%')
    .height('100%')
    .padding(10)
  }
}
```



## 三、UI设计

### (1)放置九宫格

首先放入一个九宫格，设置参数

![1](Image/2.png)

```ts
      PatternLock(this.patternLockController)
        .sideLength(350)            // 设置宽高
        .circleRadius(15)            // 设置圆点半径
        .regularColor(Color.Blue)    // 设置圆点颜色
        .pathStrokeWidth(30)        // 设置连线粗细
        .backgroundColor('rgba(209, 219, 229, 0.95)')
        .autoReset(true)            // 支持用户在完成输入后再次触屏重置组件状态
        .border({radius:30})
        .onPatternComplete((input: Array<number>) => {
        })
```

### （2）放置按钮

![1](Image/3.png)

```ts
            Button('清除')
              .width(200)
              .fontSize(20)
              .onClick(() => {
                this.patternLockController.reset();
              })
```

### （3）设置密码

![1](Image/4.png)

```ts
 @State passwords: Number[] = []

      Text(this.passwords.toString())
        .textAlign(TextAlign.Center)
        .fontSize(22)

        .onPatternComplete((input: Array<number>) => {
          if (input == null || input == undefined || input.length < 5) {
            this.message = "密码长度至少为5位数。";
            return;
          }
          if (this.passwords.length > 0) {
            if (this.passwords.toString() == input.toString()) {
              this.passwords = input
              this.message = "密码设置成功"
            } else {
              this.message = '密码输入错误'
            }
          } else {
            this.passwords = input
            this.message = "密码输入错误"
          }
        })
```

## （4）修改密码

![1](Image/5.png)

```ts
      Button('重置密码')
        .width(200)
        .fontSize(20)
        .onClick(() => {
          set_flag = 1;
          this.passwords = [];
          this.message = '请输入新的密码';
          this.patternLockController.reset();
        })
```

## 四、效果展示

![1](Image/2.gif)

设置密码

![1](Image/3.gif)

密码错误

![1](Image/4.gif)

密码正确
