#  #深入浅出学习eTs#（十三）滑动块提示

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、需求分析

![1](Image/1.gif)

在打开APP的时候有时候会跳出来以上这种指引我们操作的滑块，需要一直点击下一步下一步，我们本章来制作一个这种：

* 进入APP弹出
* 需要点击下一步更换

## 二、控件介绍

# Swiper：[官方文档]([OpenAtom OpenHarmony](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-container-swiper.md/))

滑块视图容器，提供子组件滑动轮播显示的能力。

> **说明：**
>
> 该组件从API Version 7开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

## 接口

Swiper(controller?: SwiperController)

**参数：**

| 参数名     | 参数类型                                                     | 必填 | 参数描述                                 |
| :--------- | :----------------------------------------------------------- | :--- | :--------------------------------------- |
| controller | [SwiperController](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-container-swiper.md/#swipercontroller) | 否   | 给组件绑定一个控制器，用来控制组件翻页。 |

## 属性

除支持[通用属性](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-size.md/)外，还支持以下属性，不支持[Menu控制](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-menu.md/)。

| 名称             | 参数类型                                                     | 描述                                                         |
| :--------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| index            | number                                                       | 设置当前在容器中显示的子组件的索引值。 默认值：0             |
| autoPlay         | boolean                                                      | 子组件是否自动播放，自动播放状态下，导航点不可操作。 默认值：false |
| interval         | number                                                       | 使用自动播放时播放的时间间隔，单位为毫秒。 默认值：3000      |
| indicator        | boolean                                                      | 是否启用导航点指示器。 默认值：true                          |
| loop             | boolean                                                      | 是否开启循环。 设置为true时表示开启循环，在LazyForEach懒循环加载模式下，加载的组件数量建议大于5个。 默认值：true |
| duration         | number                                                       | 子组件切换的动画时长，单位为毫秒。 默认值：400               |
| vertical         | boolean                                                      | 是否为纵向滑动。 默认值：false                               |
| itemSpace        | number \| string                                             | 设置子组件与子组件之间间隙。 默认值：0                       |
| displayMode      | SwiperDisplayMode                                            | 主轴方向上元素排列的模式，优先以displayCount设置的个数显示，displayCount未设置时本属性生效。 默认值：SwiperDisplayMode.Stretch |
| cachedCount8+    | number                                                       | 设置预加载子组件个数。 默认值：1                             |
| disableSwipe8+   | boolean                                                      | 禁用组件滑动切换功能。 默认值：false                         |
| curve8+          | [Curve](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-appendix-enums.md/#curve) \| string | 设置Swiper的动画曲线，默认为淡入淡出曲线，常用曲线参考[Curve枚举说明](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-appendix-enums.md/#curve)，也可以通过插值计算模块提供的接口创建自定义的Curves([插值曲线对象](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-interpolation-calculation.md/))。 默认值：Curve.Ease |
| indicatorStyle8+ | { left?: [Length](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#length), top?: [Length](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#length), right?: [Length](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#length), bottom?: [Length](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#length), size?: [Length](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#length), mask?: boolean, color?: [ResourceColor](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/), selectedColor?: [ResourceColor](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/) } | 设置导航点样式： - left: 设置导航点距离Swiper组件左边的距离。 - top: 设置导航点距离Swiper组件顶部的距离。 - right: 设置导航点距离Swiper组件右边的距离。 - bottom: 设置导航点距离Swiper组件底部的距离。 - size: 设置导航点的直径。 - mask: 设置是否显示导航点蒙层样式。 - color: 设置导航点的颜色。 - selectedColor: 设置选中的导航点的颜色。 |
| displayCount8+   | number\|string                                               | 设置元素显示个数。 默认值：1                                 |
| effectMode8+     | [EdgeEffect](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-appendix-enums.md/#edgeeffect) | 滑动效果，目前支持的滑动效果参见EdgeEffect的枚举说明。 默认值：EdgeEffect.Spring |

```js
// xxx.ets
class MyDataSource implements IDataSource {
  private list: number[] = []
  private listener: DataChangeListener

  constructor(list: number[]) {
    this.list = list
  }

  totalCount(): number {
    return this.list.length
  }

  getData(index: number): any {
    return this.list[index]
  }

  registerDataChangeListener(listener: DataChangeListener): void {
    this.listener = listener
  }

  unregisterDataChangeListener() {
  }
}

@Entry
@Component
struct SwiperExample {
  private swiperController: SwiperController = new SwiperController()
  private data: MyDataSource = new MyDataSource([])

  aboutToAppear(): void {
    let list = []
    for (var i = 1; i <= 10; i++) {
      list.push(i.toString());
    }
    this.data = new MyDataSource(list)
  }

  build() {
    Column({ space: 5 }) {
      Swiper(this.swiperController) {
        LazyForEach(this.data, (item: string) => {
          Text(item).width('90%').height(160).backgroundColor(0xAFEEEE).textAlign(TextAlign.Center).fontSize(20)
        }, item => item)
      }
      .cachedCount(2)
      .index(1)
      .autoPlay(true)
      .interval(4000)
      .indicator(true) // 默认开启指示点
      .loop(false) // 默认开启循环播放
      .duration(1000)
      .vertical(false) // 默认横向切换
      .itemSpace(0)
      .curve(Curve.Linear) // 动画曲线
      .onChange((index: number) => {
        console.info(index.toString())
      })

      Flex({ justifyContent: FlexAlign.SpaceAround }) {
        Button('next')
          .onClick(() => {
            this.swiperController.showNext()
          })
        Button('preview')
          .onClick(() => {
            this.swiperController.showPrevious()
          })
      }
    }.margin({ top: 5 })
  }
}

```

## 三、UI设计

### （1）首先需要放置一个滑动组件，如下图：

![1](Image/6.png)

```js
      Swiper(this.swiperController) {
        Image($r('app.media.1')).objectFit(ImageFit.Contain)
        Image($r('app.media.4')).objectFit(ImageFit.Contain)
        Image($r('app.media.2')).objectFit(ImageFit.Contain)
        Image($r('app.media.3')).objectFit(ImageFit.Contain)
        Image($r('app.media.5')).objectFit(ImageFit.Contain)
      }
      .index(0)
      .cachedCount(5)
      .index(1)
      .indicator(true) // 默认开启指示点
      .loop(true) // 默认开启循环播放
      .duration(200)
      .vertical(false) // 默认横向切换
      .itemSpace(0)
      .curve(Curve.Linear) // 动画曲线
      .onChange((index: number) => {
        console.info(index.toString())
      })
      .backgroundColor(Color.Gray)
      .width("100%")
      .height("40%")
```

## （2）接下来放置按钮

按钮与滑动的控制器进行绑定

![1](Image/7.png)

内容选择为下一条，上一条

```js
Row()
{
  Button('下一条')
    .onClick(() => {
      this.swiperController.showNext()
    })
  Blank()
    .width("20%")
  Button('上一条')
    .onClick(() => {
      this.swiperController.showPrevious()
    })
}
```

## (3)导入图片

这里的图片选取的是51CTO官网的一些轮播图片

![1](Image/8.png)

选择图片资源，进行导入

```js
      Swiper(this.swiperController) {
        Image($r('app.media.1')).objectFit(ImageFit.Contain)
        Image($r('app.media.4')).objectFit(ImageFit.Contain)
        Image($r('app.media.2')).objectFit(ImageFit.Contain)
        Image($r('app.media.3')).objectFit(ImageFit.Contain)
        Image($r('app.media.5')).objectFit(ImageFit.Contain)
      }
```

![1](Image/9.png)

## 四、成果展示

![1](Image/2.gif)
