# #深入浅出学习eTs#（一）模拟器/真机环境搭建

本项目的Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、下载DevEco到电脑（Windows）

本系列所有内容都是基于windows下使用，如需要Linux下的开发，请类比参考，这里不做具体说明

首先打开华为官方网址：[HUAWEI DevEco Studio和SDK下载和升级 | HarmonyOS开发者](https://developer.harmonyos.com/cn/develop/deveco-studio#download)

这里选择DevEco Studio 3.0 Release，即993版本，可以同时开发Harmony和OpenHarmony的应用，这里主要是eTs下OpenHarmony的开发

![5](Image/5.png)

点击后开始下载，下载完成后打开

## 二、安装DecEco

![5](Image/1.png)

在安装时选择对应的路径，其中需要下载Harmony和openharmony的SDK路径，个人建议都放置在相同的路径方便管理

![5](Image/2.png)

上方列出了需要安装的SDK目录

![5](Image/3.png)

在这个界面记得需要点两个同意（Accept）即按左边的OHS和HS，这样才能选择Next进入下一步

![5](Image/4.png)

之后提示安装成功，进行打开

## 三、创建基础Demo

![5](Image/6.png)

打开DevEco后选择创建工程

![5](Image/7.png)

在这里我们选择OpenHarmony版本的空页面进行开发

![5](Image/8.png)

在这个页面可以进行基础的配置相关内容（包括要编译的程序名称、版本号等等）

还可以选择eTs的版本，这里一般使用API8进行开发，之后如果用到相机的部分会额外说明

此处还有个面向新人的特别方便的图形化界面（Super Visual）,这个之后会详细说明

![5](Image/9.png)

点击创建后，稍作等待会进入这个界面，此时的Index.ets就是我们的主文件了，resources则是资源文件（之后会详细说明）

初始化的界面内容是一个“Hello world”,做为程序猿不可避免的一部分的纯在

![5](Image/10.png)

我们选中ets文件，此时点击右边的预览，则可以进入模拟器查看我们编写的界面

## 四、实机开发

实机开发这里分两种，一个是Harmony设备（需要3.0版本），另一个是OpenHarmony设备，这里以DAYU200为例

（1）Harmony实机开发

[创建和运行Hello World-快速开始-DevEco Studio使用指南(HarmonyOS)-工具-HarmonyOS应用开发](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/hello_world-0000001054516888?spm=a2c6h.12873639.article-detail.10.5bf16319CCr3RH)

详见官方文档

（2）DAYU200开发

[DAYU200最新烧录OpenHarmony系统教程-开源基础软件社区-51CTO.COM](https://ost.51cto.com/posts/12865)



#### 请大家关注我！！这个系列会深入浅出的讲解eTs在OpenHarmony上的开发，并不定时做一些开源小游戏，总系列预计40+章节！！

#### 请大家关注我！！这个系列会深入浅出的讲解eTs在OpenHarmony上的开发，并不定时做一些开源小游戏，总系列预计40+章节！！

#### 请大家关注我！！这个系列会深入浅出的讲解eTs在OpenHarmony上的开发，并不定时做一些开源小游戏，总系列预计40+章节！！