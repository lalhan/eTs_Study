#  #深入浅出学习eTs#（三）UI布局

#### 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、ArkUI介绍

## 框架介绍

方舟开发框架（简称：ArkUI），是一套UI开发框架，提供开发者进行应用UI开发时所必需的能力。

## 基本概念

- 组件：组件是界面搭建与显示的最小单位。开发者通过多种组件的组合，构建出满足自身应用诉求的完整界面。
- 页面：page页面是方舟开发框架最小的调度分割单位。开发者可以将应用设计为多个功能页面，每个页面进行单独的文件管理，并通过路由API实现页面的调度管理，以实现应用内功能的解耦。

## 主要特征

- UI组件：方舟开发框架不仅提供了多种基础组件，如文本显示、图片显示、按键交互等，也提供了支持视频播放能力的媒体组件。并且针对不同类型设备进行了组件设计，提供了组件在不同平台上的样式适配能力，此种组件称为“多态组件”。
- 布局：UI界面设计离不开布局的参与。方舟开发框架提供了多种布局方式，不仅保留了经典的弹性布局能力，也提供了列表、宫格、栅格布局和适应多分辨率场景开发的原子布局能力。
- 动画：方舟开发框架对于UI界面的美化，除了组件内置动画效果外，也提供了属性动画、转场动画和自定义动画能力。
- 绘制：方舟开发框架提供了多种绘制能力，以满足开发者绘制自定义形状的需求，支持图形绘制、颜色填充、文本绘制、图片绘制等。
- 交互事件：方舟开发框架提供了多种交互能力，满足应用在不同平台通过不同输入设备均可正常进行UI交互响应，默认适配了触摸手势、遥控器、鼠标等输入操作，同时也提供事件通知能力。
- 平台API通道：方舟开发框架提供了API扩展机制，平台能力通过此种机制进行封装，提供风格统一的JS接口。

## 二、常见布局

# 弹性布局

Flex组件用于创建弹性布局，开发者可以通过Flex的接口创建容器组件，进而对容器内的其他元素进行弹性布局，例如：使三个元素在容器内水平居中，垂直等间隔分散。

# 栅格布局

栅格系统作为一种辅助布局的定位工具，在平面设计和网站设计都起到了很好的作用，对移动设备的界面设计有较好的借鉴作用。总结栅格系统对于移动设备的优势主要有：

1. 给布局提供一种可循的规律，解决多尺寸多设备的动态布局问题。
2. 给系统提供一种统一的定位标注，保证各模块各设备的布局一致性。
3. 给应用提供一种灵活的间距调整方法，满足特殊场景布局调整的可能性。

# 媒体查询

媒体查询（Media Query）在移动设备上应用十分广泛，开发者经常需要根据设备的大致类型或者特定的特征和设备参数（例如屏幕分辨率）来修改应用的样式。为此媒体查询提供了如下功能：

1. 针对设备和应用的属性信息，可以设计出相匹配的布局样式。
2. 当屏幕发生动态改变时（比如分屏、横竖屏切换），应用页面布局同步更新。

### 本教程主要涉及弹性布局和栅格布局的讲解和演示

## 三、弹性布局

# Flex：[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-container-flex.md/)

应用弹性方式布局子组件的容器组件。

- 参数

  | 参数名         | 参数类型                                                     | 必填 | 默认值            | 参数描述                                                     |
  | :------------- | :----------------------------------------------------------- | :--- | :---------------- | :----------------------------------------------------------- |
  | direction      | FlexDirection                                                | 否   | FlexDirection.Row | 子组件在Flex容器上排列的方向，即主轴的方向。                 |
  | wrap           | FlexWrap                                                     | 否   | FlexWrap.NoWrap   | Flex容器是单行/列还是多行/列排列。                           |
  | justifyContent | FlexAlign                                                    | 否   | FlexAlign.Start   | 子组件在Flex容器主轴上的对齐格式。                           |
  | alignItems     | [ItemAlign](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-appendix-enums.md/#itemalign枚举说明) | 否   | ItemAlign.Stretch | 子组件在Flex容器交叉轴上的对齐格式。                         |
  | alignContent   | FlexAlign                                                    | 否   | FlexAlign.Start   | 交叉轴中有额外的空间时，多行内容的对齐方式。仅在wrap为Wrap或WrapReverse下生效。 |

在Super Visual中导入一个flex布局，在右侧可以看到如上的参数

![1](Image/1.png)

* FlexDirection枚举说明

| 名称          | 描述                           |
| :------------ | :----------------------------- |
| Row           | 主轴与行方向一致作为布局模式。 |
| RowReverse    | 与Row方向相反方向进行布局。    |
| Column        | 主轴与列方向一致作为布局模式。 |
| ColumnReverse | 与Column相反方向进行布局。     |

在flex中放置两个标签，通过修改该参数可以实现横向和竖向的显示

![1](Image/2.png)

![1](Image/3.png)

如上图，是direction的演示，Flex还有其它参数

- FlexWrap枚举说明

  | 名称        | 描述                                              |
  | :---------- | :------------------------------------------------ |
  | NoWrap      | Flex容器的元素单行/列布局，子项不允许超出容器。   |
  | Wrap        | Flex容器的元素多行/列排布，子项允许超出容器。     |
  | WrapReverse | Flex容器的元素反向多行/列排布，子项允许超出容器。 |

- FlexAlign枚举说明

  | 名称         | 描述                                                         |
  | :----------- | :----------------------------------------------------------- |
  | Start        | 元素在主轴方向首端对齐，第一个元素与行首对齐，同时后续的元素与前一个对齐。 |
  | Center       | 元素在主轴方向中心对齐，第一个元素与行首的距离与最后一个元素与行尾距离相同。 |
  | End          | 元素在主轴方向尾部对齐，最后一个元素与行尾对齐，其他元素与后一个对齐。 |
  | SpaceBetween | Flex主轴方向均匀分配弹性元素，相邻元素之间距离相同。第一个元素与行首对齐，最后一个元素与行尾对齐。 |
  | SpaceAround  | Flex主轴方向均匀分配弹性元素，相邻元素之间距离相同。第一个元素到行首的距离和最后一个元素到行尾的距离是相邻元素之间距离的一半。 |
  | SpaceEvenly  | Flex主轴方向元素等间距布局，相邻元素之间的间距、第一个元素与行首的间距、最后一个元素到行尾的间距都完全一样。 |

  ### 大家可以自己自由测试，这里就不一一演示了

## 四、栅格布局

## 栅格系统

栅格系统有Column、Margin、Gutter三个概念。

![1](Image/4.png)

1. Gutter： 用来控制元素与元素之间距离关系。可以根据设备的不同尺寸，定义不同的gutter值，作为栅格布局的统一规范。为了保证较好的视觉效果，通常gutter的取值不会大于margin的取值。
2. Margin： 离栅格容器边缘的距离。可以根据设备的不同尺寸，定义不同的margin值，作为栅格布局的统一规范。
3. Column: 栅格布局的主要定位工具。根据设备的不同尺寸，把栅格容器分割成不同的列数，在保证margin和gutter符合规范的情况下，根据总Column的个数计算每个Column列的宽度。

### 系统栅格断点

系统根据不同水平宽度设备对应Column的数量关系，形成了一套断点规则定义。

系统以设备的水平宽度的屏幕密度像素值作为断点依据，根据当前设备水平宽度所在的断点范围，定义了设备的宽度类型。系统的栅格断点范围、设备宽度类型及其描述，以及对应的默认总列数(column)，边距（margin)，间隔(gutter)定义如下：

| 设备水平宽度断点范围  | 设备宽度类型 | 描述               | columns | gutter | margin |
| :-------------------- | :----------- | :----------------- | :------ | :----- | :----- |
| 0<水平宽度<320vp      | XS           | 最小宽度类型设备。 | 2       | 12vp   | 12vp   |
| 320vp<=水平宽度<600vp | SM           | 小宽度类型设备。   | 4       | 24vp   | 24vp   |
| 600vp<=水平宽度<840vp | MD           | 中等宽度类型设备。 | 8       | 24vp   | 32vp   |
| 840<=水平分辨率       | LG           | 大宽度类型设备。   | 12      | 24vp   | 48vp   |

### 这里以两层嵌套为例：形成一个田字格

![1](Image/5.png)

#### 中间是经过多层嵌套形成的田字格，左边是如何进行嵌套的层级展示，Row为竖向，Column为横向，如果大家要做一个计算器或者任意形式的布局，使用Row和Column基本都可以实现
