#  #深入浅出学习eTs#（八）“赌博”小游戏

#### 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

![1](Image/1.png)

##### 上一章节提到的模拟器存在的BUG问题，目前没有办法直接改善，本来打算直接使用鸿蒙远程设备来实现，但是发现支持API8的设备都被抢光了（包括模拟器），而本地模拟器仅仅都支持API6，也是不能使用的，在之后的内容中咱们还是依托于预览器来实现，如果实现不了的或者有特定需求的，我会使用DAYU200真机来实现

## 一、基本需求

![1](Image/2.png)

本章节给大家带来一个最基础的一个赌博小游戏，即通过猜大猜小，然后使用随机数来进行判定。

![1](Image/7.png)

## 二、控件介绍

本章节中使用到的新控件为进度条和图片（代码），用来展示开奖的这个过程

# Progress：[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-progress.md/)

> **说明：** 该组件从API Version 7开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

进度条，用于显示内容加载或操作处理进度。

```java
interface ProgressInterface {
  (options: ProgressOptions): ProgressAttribute;
}

declare interface ProgressOptions {
  value: number; // 必须要指定初始进度
  total?: number;
  style?: ProgressStyle
  type?: ProgressType
}
```

参数

| 参数名 | 参数类型     | 必填 | 默认值              | 参数描述         |
| :----- | :----------- | :--- | :------------------ | :--------------- |
| value  | number       | 是   | -                   | 指定当前进度值。 |
| total  | number       | 否   | 100                 | 指定进度总长。   |
| type   | ProgressType | 否   | ProgressType.Linear | 指定进度条样式。 |

![1](Image/1.gif)

主要有以上几种样式，我们在这里使用最基础的长条形来进行使用，

# Image

图片组件，支持本地图片和网络图片的渲染展示。

> **说明：** 该组件从API Version 7开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

## 权限说明

使用网络图片时，需要在config.json（FA模型）或者module.json5（Stage模型）对应的"abilities"中添加网络使用权限ohos.permission.INTERNET。

```
"abilities": [
  {
    ...
    "permissions": ["ohos.permission.INTERNET"],
    ...
  }
] 
```

```java
interface ImageInterface {
  (src: string | PixelMap | Resource): ImageAttribute;
}
src：设置要加载的图片资源，支持从本地、网络和内存中加载图片，简单样例如下：
    Image($r("app.media.test"))
  .width(180)
  .height(80)
```



## 三、UI设计

首先将图片放入目录当中

![1](Image/3.png)

此时在我们的Image控件当中就可以使用媒体的路径地址：$r("app.media.2")

```java
        Image($r("app.media.2"))
          .width(300)
          .height(300)
```

此时可以在右侧预览器中显示出来我们的图片

![1](Image/4.png)

在图片下面需要加入两个按钮，分别是赌大和赌小

```java
 Row() {
          Button('赌大')
            .width(150)
            .onClick(() => {

            })
          Button('赌小')
            .width(150)
            .onClick(() => {
            })
        }
```

因为是需要水平排列，这里使用Row容器组件，写入点击事件备用

![1](Image/5.png)

在按钮下边则加入我们的进度条控件

```java
         @State Set_Num: number = 0;
Progress({
          value: this.Set_Num,                   // 设置当前进度
          total: 100,                  // 设置进度总量
          type: ProgressType.Linear    // 设置进度条的样式为条形样式
        })
          .size({width: '100%', height: 40})
```

设置总宽度为100，当前宽度为0（赋值给了变量）

在最下面放置一个标签，用来提示当前系统的运行状态

![1](Image/6.png)

## 四、功能设计

首先呢，需要让我们的进度条动起来，因为没有直接的sleep函数，所以需要我们构建一个，这里引入了同步和异步的概念，怕大家理解不了就不深入展开了，应用方式如下：

```java
function sleep(ms){
  return new Promise((resolve)=>setTimeout(resolve,ms));
}

async Get_Result()
  {
    this.Set_Num = 0
    for(var i = 0;i<=100;i++)
    {
      var temple = await sleep(10);
      this.Set_Num = i
    }
}
```

上面的Promise 和 下面的async为对应出现，因为我们把进度条的当前位置赋值给了 this.Set_Num，那么上面的部分意思就是在for循环中增加进度条的当前位置，间隔为10ms，一共100次，则对应进度条1s走完，实现一个动画效果

![1](Image/2.gif)

在完成动画效果后，我们需要实现“赌博”功能，即实现对赌大赌小的分析，这里使用随机数生成器

```java
this.The_Result = Math.ceil(Math.random() * 100 + 1)
```

将结果赋值给this.The_Result(0-100大小的数)

此时我们判断按下的是赌大还是赌小，分别进行分析

```java

async Get_Result()
  {
    this.Set_Num = 0
    for(var i = 0;i<=100;i++)
    {
      var temple = await sleep(10);
      this.Set_Num = i
    }
    this.The_Result = Math.ceil(Math.random() * 100 + 1)
    if(this.Flag){
      if(this.The_Result<50)
      {
        this.message = '你输了！！' + this.The_Result.toString()
      }else
      {
        this.message = '你赢了！！'+ this.The_Result.toString()
      }

    }else
    {
      if(this.The_Result>=50)
      {
        this.message = '你输了！！'+ this.The_Result.toString()
      }else
      {
        this.message = '你赢了！！'+ this.The_Result.toString()
      }

    }
  }

```

补充完整按键部分的程序

```java
        Row() {
          Button('赌大')
            .width(150)
            .onClick(() => {
              this.Flag = 1
              this.Get_Result()

            })
          Button('赌小')
            .width(150)
            .onClick(() => {
              this.Flag = 0
              this.Get_Result()
            })
        }
```

此时便能实现完整的一次运行，在运行后会将结果以及随机数的大小显示在标签上

## 五、功能演示

![1](Image/3.gif)

如上图，实现了预计功能
