#  #深入浅出学习eTs#（十一）别忘了吃药喔

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、需求分析

<img src="Image/1.png" alt="1" style="zoom:33%;" />

我们本章节要实现一个闹钟功能，实现闹钟定时，提醒大家吃药（最好不需要吃药喔），功能分析：

* 时间选择控件
* 类似手机的闹钟UI
* 滚动条播放消息
* 设定后进行提示

## 二、控件介绍

这里我们要用到的是时间选择控件：

# TimePicker：[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-timepicker.md/)

滚动选择时间的组件。

> **说明：** 该组件从API Version 8开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

```ts
TimePicker 是选择时间的滑动选择器组件，默认以 00:00 至 23:59 的时间区创建滑动选择器
  interface TimePickerInterface {
  (options?: TimePickerOptions): TimePickerAttribute;
}

declare interface TimePickerOptions {
  selected?: Date;
}
```

简单使用样例：

```ts
TimePicker({selected: new Date()}) // 设置默认当前时间
  .width(200)
  .height(120)
  .backgroundColor('#aabbcc')
```

# Marquee:[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-marquee.md/)

> **说明：** 该组件从API Version 8开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

跑马灯组件，用于滚动展示一段单行文本，仅当文本内容宽度超过跑马灯组件宽度时滚动。

## 接口

Marquee(value: { start: boolean, step?: number, loop?: number, fromStart?: boolean, src: string })

- 参数

  | 参数名    | 参数类型 | 必填 | 默认值 | 参数描述                                   |
  | :-------- | :------- | :--- | :----- | :----------------------------------------- |
  | start     | boolean  | 是   | -      | 控制是否进入播放状态。                     |
  | step      | number   | 否   | 6      | 滚动动画文本滚动步长。                     |
  | loop      | number   | 否   | -1     | 设置重复滚动的次数，小于等于零时无限循环。 |
  | fromStart | boolean  | 否   | true   | 设置文本从头开始滚动或反向滚动。           |
  | src       | string   | 是   | -      | 需要滚动的文本。                           |

![1](Image/1.gif)

## 三、UI设计

### （1）背景颜色

我想实现一个纯白好看的界面，这次选择使用灰色打底

```ts
    Row(){}
	}.width('100%')
    .height('100%')
    .backgroundColor('rgba(241, 243, 245, 0.95)')
```

并且我们本次要做一个横屏下的开发，如何在预览器中切换横竖屏（看下图）

![1](Image/2.png)

### （2）滚动条

```ts
        Marquee({
          start: true,
          step: 36,
          loop: -1,
          fromStart: true,
          src: '请大家填写闹钟信息'
        })
          .width(600)
          .height(50)
          .fontSize(35)
          .allowScale(false)
          .fontWeight(FontWeight.Bold)
          .backgroundColor(Color.White)
          .margin({bottom:40,left:100})
          .borderRadius(30)
```

在这里引入了圆角的属性borderRadius：可以把矩形变为圆角矩形，更贴近圆形的UI更符合当前的科技发展与生活

![1](Image/2.gif)

### （3）时间选择条

```ts
        Row() {
          Text('设置闹钟时间（24小时）：').fontSize(20)
          Blank()
          TimePicker({
          })
            .width('100vp')
            .height('100vp')
            .useMilitaryTime(true)
            .onChange((date: TimePickerResult) => {

            })
          Text('分').fontSize(18)
          Toggle({ type: ToggleType.Switch })

        }.width('500vp').backgroundColor(0xFFFFFF).padding({ left: 15 }).borderRadius(30)
        .margin({top:30})
```

这里放置了提示按钮，中间间距放置了空的内容，后面为时间选择器，之后是之前学过的开关器件

![1](Image/3.png)

### （4）消息提示

我们这里选择使用Toast来实现交互提示

```ts
declare namespace prompt {
  // 显示一个Toast
  function showToast(options: ShowToastOptions):void;
}

interface ShowToastOptions { // Toast配置参数
  message: string;           // Toast显示文本
  duration?: number;         // Toast显示时长
  bottom?: string | number;  // Toast距离屏幕底部距离
}
```

在本系统中，当选择好时间以后，点击开关，此时提示闹钟已经设置好

![1](Image/4.png)

## 四、系统演示

![1](Image/3.gif)

如上图，已经实现了全部内容
