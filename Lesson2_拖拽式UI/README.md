#  #深入浅出学习eTs#（二）拖拽式UI

#### 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、创建支持Super Visual的工程

![5](Image/1.png)

在这里选择直尺Super Visual的选项，调整当前路径，进入绘制界面

## 二、UI设计界面介绍

![5](Image/2.png)

在左侧是路径（即文件管理器），右侧是UI的设计窗口，可以通过直接拖动的方式实现控件绘制，但目前这里面所支持的控件比较少，包括图片、标签、输入框、进度条、横竖布局、窗口、分隔条、按钮、列表等几个类型，本次仅介绍标签和按钮

## eTS工程目录介绍

- **entry**：OpenHarmony工程模块，编译构建生成一个[HAP](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/glossary.md/#hap)包。
  - **src > main > ets**：用于存放ets源码。
  - **src > main > ets > MainAbility**：应用/服务的入口。
  - **src > main > ets > MainAbility > pages**：MainAbility包含的页面。
  - **src > main > ets > MainAbility > pages > index.ets**：pages列表中的第一个页面，即应用的首页入口。
  - **src > main > ets > MainAbility > app.ets**：承载Ability生命周期。
  - **src > main > resources**：用于存放应用/服务所用到的资源文件，如图形、多媒体、字符串、布局文件等。关于资源文件，详见[资源文件的分类](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/ui/ui-ts-basic-resource-file-categories.md/)。
  - **src > main > config.json**：模块配置文件。主要包含HAP包的配置信息、应用/服务在具体设备上的配置信息以及应用/服务的全局配置信息。具体的配置文件说明，详见[应用包结构配置文件的说明（FA模型）](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/quick-start/package-structure.md/)。
  - **build-profile.json5**：当前的模块信息 、编译信息配置项，包括buildOption、targets配置等。
  - **hvigorfile.js**：模块级编译构建任务脚本，开发者可以自定义相关任务和代码实现。
- **build-profile.json5**：应用级配置信息，包括签名、产品配置等。
- **hvigorfile.js**：应用级编译构建任务脚本。

## 三、标签和按钮控件介绍

![5](Image/3.png)

在点击控件后，右上角出现一个可移动的符号，按住这个符号即可对UI进行拖拽

![5](Image/4.png)

在点击标签后，右边选择第二个，出现Content内容，点击这个转换内容后，可以直接输入想显示的内容（此时可以直接在图中间进行显示）

![5](Image/5.png)

这里输入：输入测试，即在图中间已经更新显示

![5](Image/6.png)

![5](Image/7.png)

拖入一个按钮后（在左侧鼠标左键点住拖入右边），可以在右边进行字号、宽度、高度、粗细、类型、文本内容、背景颜色、文本颜色等等的内容的直接选择

#### 这里还有其它很多的元素，就不一一介绍了，大家可以直接上手测试，还是挺好玩的！

## 四、转换为eTs文件

![5](Image/8.png)

在简单的UI界面设计好以后，点击这里的转换符号，可以将当前的UI设计框架转化为eTs文件

![5](Image/9.png)

按照提示进行下一步

![5](Image/10.png)

提示转换成功

## 五、模拟器测试

![5](Image/11.png)

打开模拟器后，发现得到的界面就是UI设计框的界面
