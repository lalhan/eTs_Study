#  #深入浅出学习eTs#（二十）“活性”组件

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、需求分析

![1](Image/1.png)

本章节我们来制作一个程序猿笑话，《项目经理要求这里运行缓慢，好让客户给钱优化》，在这里选择使用[OpenAtom OpenHarmony](https://www.openharmony.cn/mainPlay)新上的一个组件来实现，并且通过动态式更新组件宽度和高度的方式，在同一个页面实现不同的呈现效果

* 页面跳转
* 动态调整组件
* 实现反应快慢的调节
* 本章节于#深入浅出学习eTs#（七）判断密码是否正确上修改

## 二、控件介绍

### （1）LoadingProgress[OpenAtom OpenHarmony](https://docs.openharmony.cn/pages/v3.1/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-loadingprogress.md/)

用于显示加载进展。

> **说明：**
>
> 该组件从API Version 8开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

#### 接口

LoadingProgress()

创建加载进展组件。

#### 属性

| 名称  | 参数类型                                                     | 描述                   |
| :---- | :----------------------------------------------------------- | :--------------------- |
| color | [ResourceColor](https://docs.openharmony.cn/pages/v3.1/zh-cn/application-dev/reference/arkui-ts/ts-types.md/#resourcecolor8) | 设置加载进度条前景色。 |

![1](Image/1.gif)

```ts
// xxx.ets
@Entry
@Component
struct LoadingProgressExample {
  build() {
    Column({ space: 5 }) {
      Text('Orbital LoadingProgress ').fontSize(9).fontColor(0xCCCCCC).width('90%')
      LoadingProgress()
        .color(Color.Blue)
    }.width('100%').margin({ top: 5 })
  }
}

```

### （2）页面路由

- 本模块首批接口从API version 8开始支持。后续版本的新增接口，采用上角标单独标记接口的起始版本。
- 页面路由需要在页面渲染完成之后才能调用，在onInit和onReady生命周期中页面还处于渲染阶段，禁止调用页面路由方法。

#### 1、导入模块

```
import router from '@ohos.router'
```

### 2、router.push（跳转到应用内的指定页面。）

```ts
            router.push({          // 使用push入栈一个新页面
                url: "pages/USED"  // 通过url指定新打开的页面
              })
```

### 3、router.replace（用应用内的某个页面替换当前页面，并销毁被替换的页面）

```ts
// 在当前页面中
export default {
  replacePage() {
    router.replace({
      url: 'pages/detail/detail',
      params: {
        data1: 'message',
      },
    });
  }
}
```

### 4、router.back（返回上一页面或指定的页面）

```ts
export default {    
  indexPushPage() {        
    router.push({            
      url: 'pages/detail/detail',        
    });        
  }
}
```

### （3）定时器实现

```ts
  private Run_Get() {
    var T = setInterval(() => {
      if (this.Num == 0) {
        clearTimeout(T)
      }
    }, 1000)
  }
```



## 三、UI设计

### （1）动态组件

这里选择使用变量来充当组件宽度和高度的方式，首先新建变量

```ts
  @State LO_H: string = '400'
  @State LO_W: string = '400'
  @State SEC: number = 5
  @State B_H: string = '0'
  @State B_w: string = '0'
```

此时在页面内放置一个LoadingProgress和一个Text

```ts
    Row() {
      Column({ space: 5 }) {

        Text("欢迎VIP用户")
          .fontSize('50')
          .width(this.B_H)
          .height(this.B_w)
        LoadingProgress()
          .color(Color.Blue)
          .width(this.LO_H)
          .height(this.LO_W)

      }.width('100%').margin({ top: 5 })
    }
    .height('100%')
```

![1](Image/2.png)

### （2）放置定时器

```ts
  private Run_Get() {
    var T = setInterval(() => {
      if (this.SEC == 0) {
        this.LO_H = '0';
        this.LO_W = '0';
        this.B_H = '200';
        this.B_w = '200';
        clearTimeout(T)
      }else
      {
        this.SEC -= 1;
      }

    }, 1000)
  }



  onPageShow(){
    this.Run_Get()
  }
```

在5s后，实现一个TXT文档显示

![1](Image/3.png)

### （3）页面跳转

```ts
         if(this.QQ == "11066")
            {
              this.Password = '登录成功'
              router.push({          // 使用push入栈一个新页面
                url: "pages/USED"  // 通过url指定新打开的页面
              })
            }else
            {
              this.Password = '登录失败'
            }
```

![1](Image/4.png)

## 四、动态显示

### （1）5秒测试

![1](Image/2.gif)

### （2）2秒测试

![1](Image/3.gif)

### （3）总结

现在程序效率优化了60%，是不是一定会给我涨工资呀
