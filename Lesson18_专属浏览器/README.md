#  #深入浅出学习eTs#（十八）专属浏览器

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、需求分析

![1](Image/1.png)

在前一章节我们学到了如何使用HarmonyOS远端模拟器，这个章节我们就来实现一个联网操作，从制作自己的一个专属浏览器做起

* 默认主页地址
* 显示当前网址
* 具有刷新功能
* 可访问真实网站

## 二、控件介绍

### （1）Web

> **说明：**
>
> - 该组件从API Version 8开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。
> - 示例效果请以真机运行为准，当前IDE预览器不支持。

提供具有网页显示能力的Web组件。

#### 需要权限

访问在线网页时需添加网络权限：ohos.permission.INTERNET，具体申请方式请参考[权限申请声明](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/security/accesstoken-guidelines.md/)。

基本定义

```ts
interface WebInterface {
  (value: WebOptions): WebAttribute;
}

declare interface WebOptions {
  src: string | Resource;
  controller: WebController;
}
```

#### 属性介绍

```ts
declare class WebAttribute extends CommonMethod<WebAttribute> {
  javaScriptAccess(javaScriptAccess: boolean): WebAttribute;
  fileAccess(fileAccess: boolean): WebAttribute;
  onlineImageAccess(onlineImageAccess: boolean): WebAttribute;
  domStorageAccess(domStorageAccess: boolean): WebAttribute;
  imageAccess(imageAccess: boolean): WebAttribute;
  mixedMode(mixedMode: MixedMode): WebAttribute;
  javaScriptProxy(javaScriptProxy: { object: object, name: string, methodList: Array<string>, controller: WebController }): WebAttribute;
  databaseAccess(databaseAccess: boolean): WebAttribute;
  userAgent(userAgent: string): WebAttribute;
  // 省略部分方法
}
```

- **javaScriptAccess**：设置是否允许执行 JS 脚本，默认为 **true** ，表示允许执行。
- **fileAccess**：设置是否开启通过 **$rawfile(filepath/filename)** 访问应用中 `rawfile` 路径的文件， 默认为 **false**，表示不启用。
- **fileFromUrlAccess**：设置是否允许通过网页中的 JS 脚本访问 **$rawfile(filepath/filename)** 的内容，默认为 **false** ，表示未启用。
- **imageAccess**：设置是否允许自动加载图片资源，默认为 **true** ，表示允许。
- **onlineImageAccess**：设置是否允许从网络加载图片资源（通过 HTTP 和 HTTPS 访问的资源），默认为 **true** ，表示允许访问。
- **domStorageAccess**：设置是否开启文档对象模型存储接口（DOM Storage API）权限，默认为 **false** ，表示未开启。
- **mixedMode**：设置是否允许加载超文本传输协议（HTTP）和超文本传输安全协议（HTTPS）混合内容，默认为 **MixedMode.None** ，表示不允许加载 HTTP 和 HTTPS 混合内容。
- **databaseAccess**：设置是否开启数据库存储 API 权限，默认为 **false** ，表示不开启。
- **userAgent**：设置用户代理。
- **javaScriptProxy**：注入 `JavaScript` 对象到 `window` 对象中，并在 `window` 对象中调用该对象的方法。所有参数不支持更新。

#### Web事件介绍

```ts
declare class WebAttribute extends CommonMethod<WebAttribute> {
  onPageBegin(callback: (event?: { url: string }) => void): WebAttribute;
  onPageEnd(callback: (event?: { url: string }) => void): WebAttribute;
  onProgressChange(callback: (event?: { newProgress: number }) => void): WebAttribute;
  onTitleReceive(callback: (event?: { title: string }) => void): WebAttribute;
  onAlert(callback: (event?: { url: string, message: string, result: JsResult }) => boolean): WebAttribute;
  onConsole(callback: (event?: { message: ConsoleMessage }) => boolean): WebAttribute;
  onErrorReceive(callback: (event?: { request: WebResourceRequest, error: WebResourceError }) => void): WebAttribute;
  onFileSelectorShow(callback: (event?: { callback: Function, fileSelector: object }) => void): WebAttribute;
}
```

- **onPageBegin**：网页开始加载时触发该回调，且只在 **主frame** 触发，iframe或者frameset的内容加载时不会触发此回调。
- **onPageEnd**：网页加载完成时触发该回调，且只在 **主frame** 触发。
- **onProgressChange**：网页加载进度变化时触发该回调，`newProgress` 的取值范围为[0 ~ 100]。
- **onTitleReceive**：网页 `document` 标题更改时触发该回调。
- **onAlert**：H5 页面内调用 `alert()` 时触发该回调。
- **onConsole**：H5 页面内调用 `console()` 方法时的回调。
- **onFileSelectorShow**：H5 页面 `input` 标签的 `type` 为 **flie** 时，点击按钮触发该回调。

### （2）权限管理

先看下官方的权限定义：https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/security/permission-list.md/

如果需要修改，请在Config.json中修改，其位置是"module"下新建"reqPermissions"，如下：

```js
    "reqPermissions": [
      {
        "name": "ohos.permission.MICROPHONE"
      },
      {
        "name": "ohos.permission.CAMERA"
      },
      {
        "name": "ohos.permission.MEDIA_LOCATION"
      },
      {
        "name": "ohos.permission.WRITE_MEDIA"
      },
      {
        "name": "ohos.permission.READ_MEDIA"
      },
      {
        "name": "ohos.permission.INTERNET"
      }
    ]
```

以上是申请了麦克风、摄像头、本地图库、媒体读写和网络访问（个别访问API使用）的权限。

## 三、UI/程序设计

本章节在上一章的基础上进行，有疑问请看第十七章，远端模拟器构建

### （1）权限添加

![2](Image/2.png)

在文件结构中选择config.json,添加互联网权限

![2](Image/3.png)

### （2）加载Web控件

![2](Image/4.png)

使用简易代码

```ts
@Entry
@Component
struct WebComponent {
  web_controller:WebController = new WebController();


  build() {
    Column() {
      Web({ src:'https://www.baidu.com/', controller:this.web_controller })
        .width('100%')
        .height('100%')
    }.width('100%')
    .height('80%')
  }
}
```

### 不知道怎么编译运行的看我上个章节！！！！

### （3）设计网页显示框

引入变量

```ts
@State url: string = 'https://www.baidu.com/'

Web({ src:this.url, controller:this.web_controller })
```

使用TextInput组件实现输入

```ts
      TextInput({
        placeholder: this.url
      }).height("5%").width("90%").fontSize(15)
```

![2](Image/5.png)

### （4）设计操作按键

这里操作按键设置包括刷新和加载两个按钮

```ts
      Row()
      {
        Button("刷新")
          .onClick(() => {
            this.web_controller.refresh();
          })

        Button("加载")
          .onClick(() => {
            this.web_controller.loadUrl({
              url: this.url
            })
          })
      }
```

![2](Image/6.png)

## 四、实际演示

![2](Image/1.gif)

我的这个笔记本性能不行（现在正在封控中），所以有点卡顿，这个不影响，在这个浏览器是可以输入Url和访问的，上方是远端模拟器的效果
