#  #深入浅出学习eTs#（十）蓝药丸还是红药丸

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、需求分析

![1](Image/1.png)

我们本章的内容选择致敬黑客帝国，如果你处于主角的立场，你会选择蓝药丸还是红药丸呢？本章节来构建一个选择器，让大家自己选择接受现实还是沉入虚拟！

* 通过开关进行状态选择
* 按下同意签署协议的声明
* 对不同的选择有不同的UI效果

## 二、控件介绍

主要使用到的是开关控件

# Toggle[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-toggle.md/)

组件提供勾选框样式、状态按钮样式及开关样式。

> **说明：** 该组件从API Version 8开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

```java
interface ToggleInterface {
  (options: { type: ToggleType; isOn?: boolean }): ToggleAttribute;
}
```

| 参数名 | 参数类型   | 必填 | 默认值 | 参数描述                                |
| :----- | :--------- | :--- | :----- | :-------------------------------------- |
| type   | ToggleType | 是   | -      | 开关类型。                              |
| isOn   | boolean    | 否   | false  | 开关是否打开，true：打开，false：关闭。 |

## 属性

| 名称             | 参数  | 默认值 | 参数描述                                                     |
| :--------------- | :---- | :----- | :----------------------------------------------------------- |
| selectedColor    | Color | -      | 设置组件打开状态的背景颜色。                                 |
| switchPointColor | Color | -      | 设置Switch类型的圆形滑块颜色。 > **说明：** > 仅对type为ToggleType.Switch生效。 |

## 事件

| 名称                                        | 功能描述                   |
| :------------------------------------------ | :------------------------- |
| onChange(callback: (isOn: boolean) => void) | 开关状态切换时触发该事件。 |

通过对“改变”事件内的程序编写，即可实现其它的互动,简单样例如下图

```java
@Entry @Component struct ToggleTest {
  build() {
    Column() {
      Toggle({type: ToggleType.Switch})
    }
    .width('100%')
    .height('100%')
  }
}
```

效果如下：

![1](Image/1.gif)

## 三、UI设计

### （1）图片框放置

本内容我们首先要搭建一个基础框架，还是使用我们的老样子布局，在最上面先放一个图片

![1](Image/2.png)

相同的操作，还是先将图片放到我们目录这个位置，这样就能调用资源文件了

![1](Image/3.png)

来放置一个image控件，这里引入一个新概念，可以使用百分比的形式来设置控件（区别于之前使用像素点）

```java
        Image($r("app.media.1"))
          .width('100%')				//使用百分比进行大小调整
          .height(240)
```

### （2）标签放置

UI界面上肯定是要放置一些提示用语的，比如说这个内容是想要干什么用，这里放置一个UI提示内容

```java
        Text('请选择你的药丸')
          .fontSize(30)
          .margin({top:10})
          .backgroundColor(Color.Gray)
          .fontColor(Color.White)
```

这里使用了字体大小、边缘间距、背景颜色、字体颜色四个属性

![1](Image/4.png)

### （3）开关放置

我们需要放置两个开关，即可以选择两种药丸，实现选择

```java
        Row()
        {
          Toggle({type: ToggleType.Switch})
            .selectedColor(Color.Red)
            .switchPointColor(Color.Red)
            .onChange((isOn: boolean) => {
              console.info('Component status:' + isOn)
            })
          Text("红色")
            .fontSize(30)
            .fontColor(Color.Red)
          Toggle({type: ToggleType.Switch})
            .selectedColor(Color.Blue)
            .switchPointColor(Color.Blue)
            .onChange((isOn: boolean) => {
              console.info('Component status:' + isOn)
            })
          Text("蓝色")
            .fontSize(30)
            .fontColor(Color.Blue)
        }
```

因为药丸想横向摆放，需要放置一个Row容器

![1](Image/5.png)

同时预置了选择触发事件，之后备用，此时已经可以进行选择操作

![1](Image/2.gif)

### （4）同意协议

同意协议是使用的开关的另一个类型版本

```java
        Row()
        {
          Toggle({type: ToggleType.Checkbox})
            .size({ width: 28, height: 28 })
          Text('我同意选择，绝不后悔！')
            .fontSize(20)
        }
```

![1](Image/7.png)

因为上下间距太短，这里扯入了一个新的控件：Blank()

```java
        Blank()
          .height(150)
        Row()
        {
          Toggle({type: ToggleType.Checkbox})
            .size({ width: 28, height: 28 })
          Text('我同意选择，绝不后悔！')
            .fontSize(20)
        }
```

![1](Image/8.png)

这样更贴近我们点击那些不得不同意的协议类型

### （5）切换动效

这里选择的动效是切换图片

```java
  @State Img_Src: Resource = $r("app.media.1")
```

此时也将图片放入指定目录

![1](Image/11.png)

然后调整程序

```java
        Row()
        {
          Toggle({type: ToggleType.Switch})
            .selectedColor(Color.Red)
            .switchPointColor(Color.Red)
            .onChange((isOn: boolean) => {
              this.Img_Src = $r("app.media.9")
            })
          Text("红色")
            .fontSize(30)
            .fontColor(Color.Red)
          Toggle({type: ToggleType.Switch})
            .selectedColor(Color.Blue)
            .switchPointColor(Color.Blue)
            .onChange((isOn: boolean) => {
              this.Img_Src = $r("app.media.10")

            })
          Text("蓝色")
            .fontSize(30)
            .fontColor(Color.Blue)
        }
```

当按下不同的开关时（选择不同的按钮时，显示不同的图片）

## 四、系统展示

![1](Image/3.gif)

#### 当你选择红色药丸时，会进入黑客帝国，准备战斗吧！！】

![1](Image/4.gif)

#### 当你选择蓝色药丸，那我们一起加入宝宝巴士，享受生活吧！！
