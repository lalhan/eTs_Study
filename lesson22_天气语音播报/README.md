#  #深入浅出学习eTs#（二十二）天气语音预报

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

# 一、需求分析

![1](Image/1.png)

本章节我们基于上节课的内容（HTTP协议），在上节课的基础上进行延伸，方便我们去理解协议，以及引入在线语音播报的功能实现以下功能：

* 文字输入城市
* 获取城市的天气状况
* 语音播放天气情况
* 图标更换

# 二、控件介绍

### （1）Video

用于播放视频文件并控制其播放状态的组件。

> **说明：**
>
> 该组件从API Version 7开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

#### 权限列表

使用网络视频时，需要申请权限ohos.permission.INTERNET。具体申请方式请参考[权限申请声明](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/security/accesstoken-guidelines.md/)。

#### 子组件

#### 接口

Video(value: {src?: string | Resource, currentProgressRate?: number | string | PlaybackSpeed, previewUri?: string | PixelMap | Resource, controller?: VideoController})

**参数：**

| 参数名              | 参数类型                                                     | 必填 | 参数描述                                                     |
| :------------------ | :----------------------------------------------------------- | :--- | :----------------------------------------------------------- |
| src                 | string \| [Resource](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/) | 否   | 视频播放源的路径，支持本地视频路径和网络路径。 支持在resources下面的video或rawfile文件夹里放置媒体资源。 支持dataability://的路径前缀，用于访问通过Data Ability提供的视频路径，具体路径信息详见[DataAbility说明](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/application-models/dataability-overview.md/)。 **说明：** 视频支持的格式是：mp4、mkv、webm、TS。 |
| currentProgressRate | number \| string \| PlaybackSpeed8+                          | 否   | 视频播放倍速。 **说明：** number取值仅支持：0.75，1.0，1.25，1.75，2.0。 默认值：1.0 \| PlaybackSpeed.Speed_Forward_1_00_X |
| previewUri          | string \| PixelMap8+ \| [Resource](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-types.md/) | 否   | 视频未播放时的预览图片路径。                                 |
| controller          | [VideoController](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-media-components-video.md/#videocontroller) | 否   | 设置视频控制器。                                             |

# 三、协议介绍

## （1）天气API

![1](Image/2.png)

和上一期一样，本系统依然是使用HTTP来实现的，这里使用了天气API来实现，通过输入地区即可获得很多天气数据

### 1.数据分析

![1](Image/3.png)

未处理的数据如上，经过json分析后得到下图

![1](Image/6.png)

在这个基础上使用上节课的内容对文本进行拆分，比如我们要获得当前温度

* ​	"tem": "6",

其数据是这样的，我们观察前面和后面"tem":"6","tem1":"13"，使用掐头去尾法可以得到当前温度6℃

### 2.数据概括

本次不打算全部显示，这里挑选几个进行展示

* 当前温度
* 当前天气
* 当日温度区间
* 温馨小提示

## （2）语音转文字API

这里选择使用搜狗的语音转文字API，url如下

* https://fanyi.sogou.com/reventondc/synthesis?text=一键三连&speed=1&lang=zh-CHS&from=translateweb&speaker=6

* text 要转换的文本
* speed 语速 1~？（我测试到15都还可以） 越大，语速越慢
* lan 语言类型 lan=en 英文 lan = zh-CHS 中文
* from 哪种方式请求的
* speaker 语音类型 1-6的数字

# 四、UI设计

## （1）天气图标

![1](Image/4.png)

寻找资源如上，但本次内容只选择云、雨、晴三个经典天气来分析，将三个图标放入目录下

![1](Image/7.png)

使用image控件进行生成

![1](Image/8.png)

```ts
Image(this.IMAGE_URL)
          .width(200)
          .height(200)
          .objectFit(ImageFit.Fill)
```

## （2）输入框

同上期内容

![1](Image/9.png)

```ts
        TextInput({ placeholder: '请输入要查询的城市', controller: this.controller })
          .placeholderColor(Color.Grey)
          .placeholderFont({ size: 25, weight: 400 })
          .caretColor(Color.Blue)
          .width('90%')
          .height(80)
          .margin(20)
          .fontSize(25)
          .fontColor(Color.Black)
          .onChange((value: string) => {
            this.IN_Value = value
            console.log(JSON.stringify(this.IN_Value));
          })
```

## （3）按钮和显示框

```ts
 Button('查  询')
          .width('60%')
          .height(60)
          .fontSize(30)
          .onClick(() => {
     
 		 });
Blank()
          .height(20)
        Text(this.Out_Value)
          .fontSize(25)
          .width('80%')
          .height(300)
          .textAlign(TextAlign.Center)
          .border({ width: 1 })
```

## （4）Vedio设计

在这里我们选择取巧的方式，使用vedio播放网络视频，只需要隐藏控制按钮，同时将控件的宽度和高度设置为1即可

音频试听：[https://tts.youdao.com/fanyivoice?word=一键三连&le=zh&keyfrom=speaker-target](https://tts.youdao.com/fanyivoice?word=%E4%B8%80%E9%94%AE%E4%B8%89%E8%BF%9E&le=zh&keyfrom=speaker-target)

大家复制上面的url放进浏览器里面可用听到

```ts
Video({
          src: this.videoSrc,
          controller: this.v_controller,
        })
          .controls(false)
          .onStart(() => {
            console.info('onStart')
          })
          .width(1)
          .height(1)
```

# 五、代码设计

## （1）HTTPS获得数据部分

我这里将我注册的API放上来了，key部分隐藏了，不能直接使用，大家去天气API那里申请个帐号就行，免费使用2000次

```ts
          httpRequest.request(
              // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。请求的参数可以在extraData中指定
              "https://v0.yiketianqi.com/api?appid=56959347&appsecret=*******&version=v61&unescape=1&city=" + this.IN_Value,
              {
                method: http.RequestMethod.GET, // 可选，默认为http.RequestMethod.GET
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
              }, (err, data) => {
              if (!err) {
                // data.result为http响应内容，可根据业务需要进行解析

                var Get_Return = data.result.toString()
                console.log(JSON.stringify(Get_Return));

```

## （2）数据拆分

该部分将返回的内容进行拆分为五个单元

```ts
//天气 当前温度 温度区间 空气质量
let Get_TQ = ''
let Get_dq = ''
let Get_wdg = ''
let Get_wdd = ''
let Get_KQZ = ''
                var Begin_Num =  Get_Return.indexOf('"wea":"')
                var Last_Num = Get_Return.lastIndexOf('","wea_img"')
                var Get_TQ = Get_Return.substring(Begin_Num+7,Last_Num)

                var Begin_Num =  Get_Return.indexOf('"tem":"')
                var Last_Num = Get_Return.lastIndexOf('","tem1"')
                var Get_dq = Get_Return.substring(Begin_Num+7,Last_Num)


                var Begin_Num =  Get_Return.indexOf('"tem1":"')
                var Last_Num = Get_Return.lastIndexOf('","tem2"')
                var Get_wdg = Get_Return.substring(Begin_Num+8,Last_Num)

                var Begin_Num =  Get_Return.indexOf('"tem2":"')
                var Last_Num = Get_Return.lastIndexOf('","win"')
                var Get_wdd = Get_Return.substring(Begin_Num+8,Last_Num)

                var Begin_Num =  Get_Return.indexOf('","air_tips":"')
                var Last_Num = Get_Return.lastIndexOf('","alarm":')
                var Get_KQZ = Get_Return.substring(Begin_Num+14,Last_Num)

                console.log(JSON.stringify(Get_TQ));
                console.log(JSON.stringify(Get_dq));
                console.log(JSON.stringify(Get_wdg));
                console.log(JSON.stringify(Get_wdd));
                console.log(JSON.stringify(Get_KQZ));
             this.Out_Value = '城市：' + this.IN_Value + '\r\n' + '天气:' + Get_TQ +'\r\n'+ '温度:' + Get_dq +'℃  '+Get_wdd+'-'+Get_wdg+'\r\n' + '温馨提示:'+Get_KQZ
```

## （3）音频播放部分

```ts
//            this.videoSrc = 'http://tts.youdao.com/fanyivoice?word=****&le=zh&keyfrom=speaker-target'
//            this.v_controller.start()
```

将****部分替换成想播报的内容就行

# 六、演示

### 注意：音频部分我尝试了模拟器和远程真机都不行，声音没法传递过来，但是实际效果是有的，这个我之前做过真机

这里选择使用上海和安阳进行演示（任意城市都可以）

![1](Image/111.gif)

![1](Image/222.gif)
