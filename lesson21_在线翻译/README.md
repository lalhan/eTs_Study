#  #深入浅出学习eTs#（二十一）在线翻译

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

# 一、需求分析

![1](Image/1.png)

本章节我们来制作中文翻译成英文的实例（运行在HarmonyOS上），通过HTTP去配合API进行实现。

* 文字输入
* HTTP协议使用
* 文字翻译

# 二、控件介绍

## （1）HTTP数据请求[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/connectivity/http-request.md/)

![1](Image/2.png)

* 请求行：代表使用POST或者GET
* 请求头：主要的一些设定参数
* 请求体：数据

### 场景介绍

应用通过HTTP发起一个数据请求，支持常见的GET、POST、OPTIONS、HEAD、PUT、DELETE、TRACE、CONNECT方法。

### 接口说明

HTTP数据请求功能主要由http模块提供。

使用该功能需要申请ohos.permission.INTERNET权限。

权限申请请参考[访问控制（权限）开发指导](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/security/accesstoken-guidelines.md/)。

涉及的接口如下表，具体的接口说明请参考[API文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/apis/js-apis-http.md/)。

| 接口名                      | 功能描述                            |
| :-------------------------- | :---------------------------------- |
| createHttp()                | 创建一个http请求。                  |
| request()                   | 根据URL地址，发起HTTP网络请求。     |
| destroy()                   | 中断请求任务。                      |
| on(type: ‘headersReceive’)  | 订阅HTTP Response Header 事件。     |
| off(type: ‘headersReceive’) | 取消订阅HTTP Response Header 事件。 |

### 使用例程

```ts

import http from '@ohos.net.http';

// 每一个httpRequest对应一个http请求任务，不可复用
let httpRequest = http.createHttp();

// 用于订阅http响应头，此接口会比request请求先返回。可以根据业务需要订阅此消息
// 从API 8开始，使用on('headersReceive', Callback)替代on('headerReceive', AsyncCallback)。 8+
httpRequest.on('headersReceive', (header) => {
    console.info('header: ' + JSON.stringify(header));
});

httpRequest.request(
    // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。请求的参数可以在extraData中指定
    "EXAMPLE_URL",
    {
        method: http.RequestMethod.POST, // 可选，默认为http.RequestMethod.GET
        // 开发者根据自身业务需要添加header字段
        header: {
            'Content-Type': 'application/json'
        },
        // 当使用POST请求时此字段用于传递内容
        extraData: {
            "data": "data to send",
        },
        connectTimeout: 60000, // 可选，默认为60s
        readTimeout: 60000, // 可选，默认为60s
    }, (err, data) => {
        if (!err) {
            // data.result为http响应内容，可根据业务需要进行解析
            console.info('Result:' + data.result);
            console.info('code:' + data.responseCode);
            // data.header为http响应头，可根据业务需要进行解析
            console.info('header:' + JSON.stringify(data.header));
            console.info('cookies:' + data.cookies); // 8+
        } else {
            console.info('error:' + JSON.stringify(err));
            // 该请求不再使用，调用destroy方法主动销毁。
            httpRequest.destroy();
        }
    }
);

```

## （2）输入框[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-textinput.md/)

![1](Image/1.gif)

```ts
 TextInput({ placeholder: 'input your word...', controller: this.controller })
        .placeholderColor(Color.Grey)
        .placeholderFont({ size: 14, weight: 400 })
        .caretColor(Color.Blue)
        .width(400)
        .height(40)
        .margin(20)
        .fontSize(14)
        .fontColor(Color.Black)
        .onChange((value: string) => {
          this.text = value
        })
```



# 三、UI设计

## （1）输入框

首先创建变量,作为输入框的控制器，并且创建一个string数组，用来接收输入框中的变量

```ts
@State IN_Value: string = '' 
controller: TextInputController = new TextInputController()
```

此时屏幕中出现一个输入框

![1](Image/3.png)

## （2）按钮

![1](Image/4.png)

```ts
        Button('翻  译')
          .width('60%')
          .height(60)
          .fontSize(30)
```

## （3）显示框

![1](Image/5.png)

在这里把要输出的幅值给TEXT

```ts
        Text(this.Out_Value)
          .fontSize(25)
          .width('80%')
          .height(100)
          .textAlign(TextAlign.Center)
          .border({ width: 1 })
```

# 四、功能设计

## （1）联网权限

![1](Image/6.png)

## （2）HTTP功能

先把最基本的实现，包括包的导入，基本函数的创建

```ts
import http from '@ohos.net.http';


// 每一个httpRequest对应一个http请求任务，不可复用
let httpRequest = http.createHttp();


// 用于订阅http响应头，此接口会比request请求先返回。可以根据业务需要订阅此消息
// 从API 8开始，使用on('headersReceive', Callback)替代on('headerReceive', AsyncCallback)。 8+
httpRequest.on('headersReceive', (header) => {


});

```

再编写功能模块

```ts
            httpRequest.request(
              // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。请求的参数可以在extraData中指定
              "http://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i=" + this.IN_Value,
              {
                method: http.RequestMethod.GET, // 可选，默认为http.RequestMethod.GET
                connectTimeout: 60000, // 可选，默认为60s
                readTimeout: 60000, // 可选，默认为60s
              }, (err, data) => {
              if (!err) {
                // data.result为http响应内容，可根据业务需要进行解析

                var Get_Return = data.result.toString()
                var Begin_Num =  Get_Return.indexOf('"tgt":"')
                var Last_Num = Get_Return.lastIndexOf('"}')
                var Get_char = Get_Return.substring(Begin_Num+7,Last_Num)
                console.log(JSON.stringify(Get_char));
                this.Out_Value = Get_char

                //console.log('hello world'.lastIndexOf('o'));



              } else {
                // 该请求不再使用，调用destroy方法主动销毁。
                httpRequest.destroy();
              }
            }
            );
```

## （3）数据处理函数

### 1、substring() 

```ts
console.log('JavaScript'.substring(0, 4)); // "Java"
console.log('JavaScript'.substring(4)) // "Script"
```

用于从原字符串取出子字符串并返回，不改变原字符串，跟slice方法很相像。它的第一个参数表示子字符串的开始位置，第二个位置表示结束位置（返回结果不含该位置

### 2、indexOf() 

```ts
console.log('hello world'.indexOf('o')); // 4
console.log('JavaScript'.indexOf('script')); // -1
```

用于确定一个字符串在另一个字符串中 第一次出现 的位置，返回结果是匹配开始的位置。如果返回`-1`，就表示不匹配

### 3、lastIndexOf()

```ts
console.log('hello world'.lastIndexOf('o')); // 7
```

从尾部开始遇到的第一次出现的位置，这个位置的返回结果是 从左边开始算起，而不是从右边开始算起。

## 五、程序验证

![1](Image/2.gif)

如上图所示，已经实现了翻译功能，在下个章节会加入翻译后发音功能

