#  #深入浅出学习eTs#（七）判断密码是否正确

#### 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、基本界面

本项目基于#深入浅出学习eTs#（四）登陆界面UI，继续进行，实现一个判断的功能

![1](Image/1.png)

## 二、控件介绍

# TextInput

可以输入单行文本并支持响应输入事件的组件。

```java
interface TextInputInterface {
  (value?: TextInputOptions): TextInputAttribute;
}

declare interface TextInputOptions {
  placeholder?: ResourceStr;
  text?: ResourceStr;
  controller?: TextInputController;
}
```

定义如上，其中placeholder代表默认显示的内容，lesson4中该部分程序如下：

```java
          TextInput({ placeholder: "我的QQ帐号" })
            .width("270vp")
            .height("50vp")
            .flexShrink(0)
          TextInput({ placeholder: "**********" })
            .width("270vp")
            .height("50vp")
            .flexShrink(0)
            .type(InputType.Password)
```

其定义也是先对控件类型进行声明，然后对基本属性进行设置

## 事件

| 名称                            | 功能描述                   |
| :------------------------------ | :------------------------- |
| onChange(value: string) => void | 输入发生变化时，触发回调。 |

其中重要的是这个内容，当内容发生改变时，需要对变量进行重新幅值

## 三、按键绑定

在确认需求后，我们要做的就是对两个输入框的内容进行判断，首先设置一个点击函数

```java
        Button("登    录")
          .width("250vp")
          .height("60vp")
          .fontSize("31fp")
          .onClick(() => {
            
          })
```

然后将两个输入框的内容进行变量绑定

```java
  @State QQ: string = '110xxxx'
  @State Password: string = '123456'
          TextInput({ placeholder: this.QQ })
            .width("270vp")
            .height("50vp")
            .flexShrink(0)
            .onChange((value: string) => {
              this.QQ = value
            })
             TextInput({ placeholder: this.Password })
            .width("270vp")
            .height("50vp")
            .flexShrink(0)
            .type(InputType.Password)
```

当按键按下后对输入框内容进行判断

```java
            if(this.QQ == "11066")
            {
              this.Password = '登录成功'
            }else
            {
              this.Password = '登录失败'
            }
```

我们这里进行一个简单的判断（似乎模拟器有BUG，不能进行输入，下个章节我看下怎么解决这个问题，这次采用直接赋值this.QQ的方式完成判断）

![1](Image/2.png)

## 四、整体测试

![1](Image/1.gif)

当输入内容为110xxxx时，点击登录，在密码框提示登录失败

![1](Image/2.gif)

当输入框为11066时，点击输入框，此时得到的反馈为登录成功
