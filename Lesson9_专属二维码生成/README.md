#  #深入浅出学习eTs#（九）变红码？专属二维码生成

## 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、需求分析

我们本章的内容是要制作一个可以随着自己想要内容而变化的一个二维码，通过输入框输入内容，实现二维码图形的改变

* 通过输入框输入内容
* 使用按钮进行改变
* 使用二维码进行显示

## 二、控件介绍

这里我们使用的是官方提供的QR控件

# QRCode

用于显示单个二维码的组件。

> **说明：** 该组件从API Version 7开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

```java
interface QRCodeInterface {
  (value: string): QRCodeAttribute;
}

QRCode('Hello, OpenHarmony')
  .width(70)
  .height(70)
```

简单样例如上图程序

## 三、UI绘制

### （1）我们首先需要建立一个输入框，同时绑定一下输入框的内容（因为预览器不能实现输入功能）

```java
          @State message: string = 'Hello OpenHarmony'
		  TextInput({
          placeholder: this.message,
        })
          .width('80%')
            
```

![1](Image/1.png)

得到上方内容，此时我们可以通过对message变量的修改实现内容的改变

### （2）接下来创建按钮

```java
        Button('点击生成二维码')
          .fontSize(20)
          .margin({top:20})
```

我们这里使用到了margin参数，该参数描述如下：

|  名称  | 参数说明                                                     | 默认值 | 描述                                                         |
| :----: | :----------------------------------------------------------- | :----- | :----------------------------------------------------------- |
| margin | { top?: Length, right?: Length, bottom?: Length, left?: Length } \| Length | 0      | 设置外边距属性。 参数为Length类型时，四个方向外边距同时生效。 |

因为我们只需要在上面实现一个移位效果，这样看起来更正常

![1](Image/2.png)

> 未移位的效果

![1](Image/3.png)

> 移位后的效果

### （3）使用QRcode控件

```java
        QRCode(this.message)
          .width(170)
          .height(170)
          .margin({bottom:20})
        TextInput({
          placeholder: this.message,
        })
          .width('80%')
        Button('点击生成二维码')
          .fontSize(20)
          .margin({top:20})
```

二维码的生成要在输入框的上面，所以这里将其放置在上部，内容比较简洁，同时使用了margin对底部进行了移位，更加美观

![1](Image/4.png)

## 四、系统测试

![1](Image/1.gif)

如上图所示，完美实现预期，后期大家可以进行调色之类，比如说健康码变红了？

![1](Image/5.png)

当然，还是希望大家永不变红，大家可以下载（在第一行有Gitee仓地址）或者按照流程来自己写一个绿码的内容。
