#  #深入浅出学习eTs#（六）编写eTs第一个控件

#### 本项目Gitee仓地址：[深入浅出eTs学习: 带大家深入浅出学习eTs (gitee.com)](https://gitee.com/lalhan/eTs_Study)

## 一、控件基本属性

在使用第一个控件前，我们需要了解一些控件都有哪些基础属性，比如说我们在Super Visual中使用过的长宽和字体大小等等，通用属性有以下这些：

| 名称           | 参数说明                                                     | 默认值                                                       | 描述                                                         |
| :------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| width          | Length                                                       | -                                                            | 设置组件自身的宽度，缺省时使用元素自身内容需要的宽度。       |
| height         | Length                                                       | -                                                            | 设置组件自身的高度，缺省时使用元素自身内容需要的高度。       |
| size           | { width?: Length, height?: Length }                          | -                                                            | 设置高宽尺寸。                                               |
| padding        | { top?: Length, right?: Length, bottom?: Length, left?: Length } \| Length | 0                                                            | 设置内边距属性。 参数为Length类型时，四个方向内边距同时生效。 |
| margin         | { top?: Length, right?: Length, bottom?: Length, left?: Length } \| Length | 0                                                            | 设置外边距属性。 参数为Length类型时，四个方向外边距同时生效。 |
| constraintSize | { minWidth?: Length, maxWidth?: Length, minHeight?: Length, maxHeight?: Length } | { minWidth: 0, maxWidth: Infinity, minHeight: 0, maxHeight: Infinity } | 设置约束尺寸，组件布局时，进行尺寸范围限制。                 |
| layoutWeight   | number                                                       | 0                                                            | 容器尺寸确定时，元素与兄弟节点主轴布局尺寸按照权重进行分配，忽略本身尺寸设置。 > **说明：** > 仅在Row/Column/Flex布局中生效。 |

通过这些可以对组件的大小，组件内，组件外以及联合边距的内容进行调整。

这里再引入一个强制位移（之后可能会多次用到）

| 名称       | 参数类型                                                     | 默认值         | 描述                                                         |
| :--------- | :----------------------------------------------------------- | :------------- | :----------------------------------------------------------- |
| align      | [Alignment](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-appendix-enums.md/#alignment枚举说明) | Center         | 设置元素内容的对齐方式，只有当设置的width和height大小超过元素本身内容大小时生效。 |
| direction  | Direction                                                    | Auto           | 设置元素水平方向的布局，可选值参照Direction枚举说明。        |
| position   | { x: Length, y: Length }                                     | -              | 使用绝对定位，设置元素锚点相对于父容器顶部起点偏移位置。在布局容器中，设置该属性不影响父容器布局，仅在绘制时进行位置调整。 |
| markAnchor | { x: Length, y: Length }                                     | { x: 0, y: 0 } | 设置元素在位置定位时的锚点，以元素顶部起点作为基准点进行偏移。 |
| offset     | { x: Length, y: Length }                                     | { x: 0, y: 0 } | 相对布局完成位置坐标偏移量，设置该属性，不影响父容器布局，仅在绘制时进行位置调整。 |

## 二、标签控件

# Text：[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-text.md/)

显示一段文本的组件。

在之前的拖拽式UI中我们已经使用过了该控件，通过修改Content属性即可达到修改文本内容的效果，他的其它属性太多了，这里可以看上面的官方文档。

![1](Image/1.png)

如上图，eTs控件的基本调用方式是

```java
interface TextInterface {
  (content?: string | Resource): TextAttribute;
}
```

即首先声明一下我们是使用的什么控件（标签、按钮、绘画等等），然后在下面通过点+内容的形式，对基本属性进行说明，如上对字体大小和背景颜色实现了说明。

其中content是显示的内容：

```java
Text("Hello, eTs")

Text('Hello, LalHan')
  .width('100%')
  .textAlign(TextAlign.Center)

Text('大家一起深入浅出学习eTs,记得关注我')
  .maxLines(1)
  .textOverflow({overflow: TextOverflow.Ellipsis})
```

实现的效果如下图：

![1](Image/2.png)

## 三、按钮控件

# Button:[官方文档](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-basic-components-button.md/)

按钮组件，可快速创建不同样式的按钮。

```java
interface ButtonInterface {
  (): ButtonAttribute;
  (options: ButtonOptions): ButtonAttribute;
  (label: ResourceStr, options?: ButtonOptions): ButtonAttribute;
}
```

其中显示内容主要由label控制

```java
Button('学习ets')
          .height(60)
          .width(200)
          .fontSize(30)
          .backgroundColor('#aabbcc')
```

## 四、组合应用

本章节把Text和Button会结合起来，实现一个点击改变数字的Demo

此时按钮需要引入一个点击属性

## 事件

| 名称                                            | 支持冒泡 | 功能描述                                            |
| :---------------------------------------------- | :------- | :-------------------------------------------------- |
| onClick(callback: (event?: ClickEvent) => void) | 否       | 点击动作触发该方法调用，event参数见ClickEvent介绍。 |

## ClickEvent对象说明

| 属性名称  | 类型                                                         | 描述                                  |
| :-------- | :----------------------------------------------------------- | :------------------------------------ |
| screenX   | number                                                       | 点击点相对于设备屏幕左边沿的X坐标。   |
| screenY   | number                                                       | 点击点相对于设备屏幕上边沿的Y坐标。   |
| x         | number                                                       | 点击点相对于被点击元素左边沿的X坐标。 |
| y         | number                                                       | 点击点相对于被点击元素上边沿的Y坐标。 |
| target8+  | [EventTarget](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/reference/arkui-ts/ts-universal-events-click.md/#eventtarget8对象说明) | 被点击元素对象。                      |
| timestamp | number                                                       | 事件时间戳。                          |

在上面的程序中加入一个onclik()

```java
        Button('学习ets')
          .height(60)
          .width(200)
          .fontSize(30)
          .backgroundColor('#aabbcc')
          .onClick(() => {
          })
```

#### 此时如果想点下按钮，更改Text的显示内容，那么需要把Text的显示内容设置为一个变量

```java
@State message: string = '点击我后改变内容'
```

![1](Image/4.png)

这个需要放置在初始化的地方

此时把更改内容的程序加入到按键中，将Text的属性給到变量

```java
        Text(this.message)
          .maxLines(2)
          .fontSize(30)
        Button('学习ets')
          .height(60)
          .width(200)
          .fontSize(30)
          .backgroundColor('#aabbcc')
          .onClick(() => {
            this.message = "已经改变了喔"
          })
```

![1](Image/5.png)

在点击后对message的变量进行修改，进行测试

## 五、测试效果

![1](Image/1.gif)

如上方测试，已经实现了动态效果
